---
author: Vous
title: Chapitre 2 - Compléments
---

## I. Paragraphe 1 

texte 1

### 1. À vous de jouer

???+ question

    Voici diverses propositions

    === "Cocher la ou les affirmations correctes"

        - [ ] Proposition 1
        - [ ] Proposition 2
        - [ ] Proposition 3
        - [ ] Proposition 4

    === "Solution"

        - :x: ~~Proposition 1~~ Optionnel : Faux car ... 
        - :white_check_mark: Proposition 2 . Optionnel : Juste car ...
        - :white_check_mark: Proposition 3 . Optionnel : Juste car ...
        - :x: ~~Proposition 4~~ Optionnel : Faux car ... 


### 2. Sous paragraphe 2

Texte 1.2

## II. Paragraphe 2 :

texte 2

### 1. Sous paragraphe 1

Texte 2.1

### 2. Sous paragraphe 2

Texte 2.2

